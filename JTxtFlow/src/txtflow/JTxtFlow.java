package txtflow;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.Timer;

/**
 *
 * @author Maciej
 */
public class JTxtFlow extends JComponent implements Serializable {
    private int squareSize = 10;
    private int widthNumberOfSquares = 30;
    private int heightNumberOfSquare = 15;
    private int width = widthNumberOfSquares*squareSize;
    private int height = heightNumberOfSquare*squareSize;
    private int [][] ledDisp;
    private String Text = "Test";
    private int delay = 300;
    private Timer timer;
    private Color colorEnabledLed = new Color(255,0,0);
    private Color colorDisabledLed = new Color(245,210,210);
    private int sizeBorder = 3;
    private Color borderColor = new Color(0, 0, 0);
    
    private PropertyChangeSupport propertySupport;
    
    public JTxtFlow() {
        reload();
    }
    
    private void reload() {
        ledDisp = new int[widthNumberOfSquares][heightNumberOfSquare];
        propertySupport = new PropertyChangeSupport(this);
        Dimension d = new Dimension(width, height);
        setMinimumSize(d);
        setPreferredSize(new Dimension(d));
        setMaximumSize(d);
        StringParseToMatrix stringParseToMatrix = new StringParseToMatrix(this.Text, this.heightNumberOfSquare);
        viewMatrix(stringParseToMatrix.getMatrix());
        this.timer = new Timer(this.delay, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                flowArray();
            }
        });
        this.timer.start();
    }
    
    
    @Override
    public void paint(Graphics g) {
        super.paintComponent(g);
        for(int i = 0; i < heightNumberOfSquare; i++)
            for(int j = 0; j < widthNumberOfSquares; j++) {
                if(ledDisp[j][i] == 1)
                    onLed(j*squareSize, i*squareSize, squareSize, true, g);
                else onLed(j*squareSize, i*squareSize, squareSize, false, g);
            }
        
        if(sizeBorder > 0) {
            g.setColor(borderColor);
            g.fillRect(0, 0, getWidth(), getSizeBorder());
            g.fillRect(0, 0, getSizeBorder(), getHeight());
            g.fillRect(0, getHeight() - getSizeBorder(), getWidth(), getSizeBorder());
            g.fillRect(getWidth() - getSizeBorder(), 0, getWidth(), getHeight());
        }
               
    }

    public Color getColorEnabledLed() {
        return colorEnabledLed;
    }

    public void setColorEnabledLed(Color colorEnabledLed) {
        this.colorEnabledLed = colorEnabledLed;
    }

    public Color getColorDisabledLed() {
        return colorDisabledLed;
    }

    public void setColorDisabledLed(Color colorDisabledLed) {
        this.colorDisabledLed = colorDisabledLed;
    }
    
    
    
    public void setText(String txt) {
        this.Text = txt;
        reload();
    }
    
    public String getText() {
        return this.Text;
    }

    public int getSquareSize() {
        return squareSize;
    }

    public void setSquareSize(int size) {
        this.squareSize = size;
        reload();
    }

    public int getWidthNumberOfSquares() {
        return widthNumberOfSquares;
    }

    public void setWidthNumberOfSquares(int widthNumberOfSquares) {
        this.widthNumberOfSquares = widthNumberOfSquares;
        reload();
    }

    public int getHeightNumberOfSquare() {
        return heightNumberOfSquare;
    }

    public void setHeightNumberOfSquare(int heightNumberOfSquare) {
        this.heightNumberOfSquare = heightNumberOfSquare;
        reload();
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
        reload();
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
        reload();
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
        reload();
    }

    public int getSizeBorder() {
        return sizeBorder;
    }

    public void setSizeBorder(int sizeBorder) {
        this.sizeBorder = sizeBorder;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }
    
    
    
    
    
    private void flowArray() {
        int[][] bufTab = new int[widthNumberOfSquares][heightNumberOfSquare];
        for(int i = 0; i < heightNumberOfSquare; i++)
            for(int j = 0; j < widthNumberOfSquares; j++) {
                if(j == (widthNumberOfSquares-1)) {
                    bufTab[0][i] = ledDisp[j][i];
                } else bufTab[j+1][i] = ledDisp[j][i];
            }
        
        this.ledDisp = bufTab;
        repaint();
    }
    
    private void onLed(int x,int y,int size,boolean f,Graphics g){
        if(f){
        g.setColor(colorEnabledLed);}
        else {
            g.setColor(colorDisabledLed);
        }
        g.fillRect(x,y,size,size);
    }
    
    
    private void setFont(char c){
        for (int i=0;i<ledDisp.length;i++){
            for(int j=0;j<ledDisp[0].length;j++) {
                if(Fonts.getFont(c)[0].length-1>=i && Fonts.getFont(c).length-1>=j) {
                    ledDisp[i][j] =Fonts.getFont(c)[j][i];
                }else {
                    ledDisp[i][j]=0;
                }
            }
        }
    }
    
    private void viewMatrix( int[][] matrix){
        for (int i=0;i<ledDisp.length;i++){
            for(int j=0;j<ledDisp[0].length;j++) {
                if(i<matrix[0].length){
                    ledDisp[i][j] = matrix[j][i];
                }else{
                    ledDisp[i][j]=0;
                
                }
            }
        }
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
    
    
}

  

  
  